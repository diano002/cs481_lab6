import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:animations/animations.dart';

class GeneralToxicity extends StatelessWidget {
  final Animation<double> transitionAnimation;

  const GeneralToxicity({
    Key key,
    this.transitionAnimation}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.red[300],
      appBar: AppBar(
        centerTitle: true,
        title: Text("General Toxicity Information",
            style: Theme.of(context).textTheme.headline2),
      ),
      body: PageTransitionSwitcher(
        transitionBuilder: (
          Widget child,
          Animation<double> primaryAnimation,
          Animation<double> secondaryAnimation,
        ) {
          return FadeThroughTransition(
            fillColor: Colors.red[300],
            child: child,
            animation: primaryAnimation,
            secondaryAnimation: secondaryAnimation,
          );
        },
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: 5,
              ),
              Card(
                margin: EdgeInsets.only(left: 10, right: 10),
                child: FittedBox(
                    fit: BoxFit.contain,
                    child: Image.asset(
                      'assets/toxic.jpg',
                    )),
              ),
              SizedBox(
                height: 10,
              ),
              Card(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          leading: Icon(
                            Icons.warning,
                            size: 40,
                            color: Colors.redAccent,
                          ),
                          title: Text(
                            "What makes a plant toxic?",
                            style: Theme.of(context).textTheme.headline2,
                          ),
                          //subtitle: Text("Nature and Nurture",
                          // style: Theme.of(context).textTheme.headline4,),
                        ),
                      )
                    ],
                  )),
              SizedBox(
                height: 10,
              ),
              Card(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          leading: Icon(
                            Icons.ac_unit,
                            size: 40,
                            color: Colors.green,
                          ),
                          title: Text(
                            "Nature",
                            style: Theme.of(context).textTheme.headline2,
                          ),
                          subtitle: Text(
                            "Some plants are inherently toxic. "
                            "Take for instance a potato, which is the member of "
                            "the nightshade family. Its leaves are extremely poisonous.",
                            style: Theme.of(context).textTheme.headline5,
                          ),
                        ),
                      )
                    ],
                  )),
              SizedBox(
                height: 10,
              ),
              Card(
                  margin: EdgeInsets.only(left: 10, right: 10),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: ListTile(
                          leading: Icon(
                            Icons.access_alarm,
                            size: 40,
                            color: Colors.purpleAccent,
                          ),
                          title: Text(
                            "Nurture",
                            style: Theme.of(context).textTheme.headline2,
                          ),
                          subtitle: Text(
                            "Some plants have toxicity introduced by human "
                            "actions. "
                            "Take for instance farming practices that spread "
                            "herbicides and pesticides on plants to make them grow. "
                            "This impacts humans who eat the plants as they ingest the same"
                            " toxins.",
                            style: Theme.of(context).textTheme.headline5,
                          ),
                        ),
                      )
                    ],
                  )),
              SizedBox(
                height: 5,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
