import 'package:cs481_toxic/DetailsScreen.dart';
import 'package:cs481_toxic/GeneralToxicity.dart';
import 'package:flutter/material.dart';
import 'package:cs481_toxic/Home.dart';
import 'dart:async';
import 'package:splashscreen/splashscreen.dart';
import 'package:animations/animations.dart';

//manual splash screen inspired by https://medium.com/@rushabh.shah065/splash-screen-flutter-create-customize-splash-screen-for-flutter-android-ios-a380a199793c
class Splash extends StatefulWidget {
  @override
  _SplashState createState() => _SplashState();
}

class _SplashState extends State<Splash> {
  final splashDelay = 6;

  @override
  void initState() {
    super.initState();

    _loadWidget();
  }

  _loadWidget() async {
    var _duration = Duration(seconds: splashDelay);
    return Timer(_duration, navigationPage);
  }

  void navigationPage() {
    Navigator.pushReplacement(
        context, MaterialPageRoute(builder: (BuildContext context) => Home()));
  }

  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Theme.of(context).accentColor,
        body: Container(
          child: Column(
            children: [
              Center(
                child: Container(
                  color: Theme.of(context).accentColor,
                  margin: const EdgeInsets.only(top: 100, bottom: 20),
                  width: 300,
                  height: 300,
                  child: Image.asset('assets/splash.png'),
                ),
              ),
              SizedBox(
                height: 50,
              ),
              SizedBox(
                width: 100,
                height: 100,
                child: CircularProgressIndicator(
                  strokeWidth: 10,
                  backgroundColor: Colors.black,
                  valueColor: new AlwaysStoppedAnimation<Color>(Colors.yellow),
                ),
              ),
            ],
          ),
        ));
    /* return SplashScreen(
      seconds: 10,
      routeName: "/",
      navigateAfterSeconds: Home(),
      //for testing description
      /*navigateAfterSeconds: DetailsScreen(plant: DetailsPlant(plantName: 'Peach', image: 'peach',
      desc: """
     This is a test description about the toxicity of peaches.
     THIS IS THE MOST TOXIC FRUIT EVER.
     If you eat just one will you die for sure guaranteed.
     Make sure to avoid if you see them in the wild ! ! !
       """, toxicity: .1, portionSize: 1,)),
      title: new Text(
        'How Toxic IS This?',
        textScaleFactor: 2,
      ),*/
      image: Image.asset('assets/star.jpg'),
      loadingText: Text("Loading"),
      backgroundColor: Colors.blue[100],
      photoSize: 75.0,
      loaderColor: Colors.blue,
    );
*/
  }
}
