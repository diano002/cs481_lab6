import 'package:flutter/material.dart';
import 'package:animations/animations.dart';
import 'package:transparent_image/transparent_image.dart';

class DetailsScreen extends StatelessWidget {
  DetailsPlant plant;

  DetailsScreen({this.plant}); //pass in from Choose A Plant

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Summary of ${plant.plantName}'),
      ),
      body: showPlant(this.plant),
    );
  }

  Widget showPlant(DetailsPlant _plant) {
    return _plant;
  }
}

class DetailsPlant extends StatefulWidget {
  String plantName;
  String image;
  String desc;
  int portionSize;
  double toxicity; //between 0 - 1.0

  DetailsPlant(
      {Key key,
      this.plantName,
      this.image,
      this.desc,
      this.toxicity,
      this.portionSize})
      : super(key: key);

  @override
  _DetailsPlantState createState() => _DetailsPlantState();
}

class _DetailsPlantState extends State<DetailsPlant> {
  double _toxicity;
  int _portionSize;

  void initState() {
    _toxicity = widget.toxicity;
    _portionSize = widget.portionSize;
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: Theme.of(context).canvasColor,
      child: Column(
        children: [
          fadeImage(widget.image),
          SizedBox(height: 10),
          info(widget.desc),
          SizedBox(height: 10),
          toxicButton(),
          SizedBox(height: 10),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              Text("Safe", style: TextStyle(color: Colors.green[900]),),
              SizedBox(width: 10,),
              Text("Dangerous", style: TextStyle(color: Colors.red[800]),),
            ],
          ),
          toxicityBar(),
          SizedBox(height: 10)
        ],
      ),
    );
  }

  Widget fadeImage(String img) {
    return Container(
        height: 230,
        width: 230,
        child: Padding(
          padding: const EdgeInsets.all(8.0),
          child: FadeInImage(
            placeholder: MemoryImage(kTransparentImage),
            fadeInCurve: Curves.easeIn,
            image: AssetImage('assets/${img}.jpg'),
          ),
        ));
  }

  Widget info(String description) {
    return Container(
      child: Card(
        color: Colors.green[50],
        margin: EdgeInsets.only(left: 10, right: 10),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            ListTile(
              leading: Icon(
                Icons.info,
                size: 40,
                color: Colors.deepPurple,
              ),
              title: Text(
                '${widget.plantName} information',
                style: Theme.of(context).textTheme.headline2,
              ),
            ),
            Text('Toxicity level: ' + widget.toxicity.toString(),
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.purple,
                )),
            Text(
                'Portion size: ' +
                    widget.portionSize.toString() +
                    ' ${widget.plantName}',
                style: TextStyle(
                  fontSize: 16,
                  fontWeight: FontWeight.bold,
                  color: Colors.green,
                )),
            Text(widget.desc, style: TextStyle(color: Colors.grey[900])),
            SizedBox(
              height: 10,
            )
          ],
        ),
      ),
    );
  }

  Widget toxicButton() {
    return Container(
      child: Column(
        children: [
          Text('Increase or decresase quantity consumed',
              style: TextStyle(
                fontSize: 16,
                fontWeight: FontWeight.bold,
                color: Colors.red[800],
              )),
          Card(
            color: Colors.green[50],
            margin: EdgeInsets.only(left: 10, right: 10),
            child: Row(
              mainAxisSize: MainAxisSize.min,
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    color: Colors.red,
                    textColor: Colors.white,
                    child: Text(
                      'Intake +',
                      style: TextStyle(fontSize: 16),
                    ),
                    onPressed: () {
                      print("BEFORE increase");
                      print(_toxicity.toString());
                      increaseToxicity(widget.toxicity);
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: RaisedButton(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(10),
                    ),
                    color: Colors.green,
                    textColor: Colors.white,
                    child: Text(
                      'Intake -',
                      style: TextStyle(fontSize: 16),
                    ),
                    onPressed: () {
                      print(_toxicity.toStringAsFixed(2));
                      decreaseToxicity(widget.toxicity);
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }

  Widget toxicityBar() {
    return Container(
      width: 300,
      child: LinearProgressIndicator(
        backgroundColor: Colors.purple[100],
        valueColor: AlwaysStoppedAnimation<Color>(Colors.purple),
        value: _toxicity,
        minHeight: 20,
      ),
    );
  }

  void increaseToxicity(double number) {
    print('Initial Toxicity bar:' + _toxicity.toStringAsFixed(2));
    print('Number coming in: ' + number.toString());
    setState(() {
      if (_toxicity > 1 || (_toxicity += number) > 1) {
        _toxicity = 1;
      } else {
        print('_toxcitity ' +
            _toxicity.toStringAsFixed(2) +
            ' plus ' +
            number.toStringAsFixed(2) +
            ' = ');
       //r _toxicity += number;
      }
    });
    print('Toxicity bar:' + _toxicity.toStringAsFixed(2));
  }

  void decreaseToxicity(double number) {
    print('Initial Toxicity bar:' + _toxicity.toStringAsFixed(2));
    setState(() {
      if (_toxicity < 0 || (_toxicity -= number) < 0) {
        _toxicity = 0;
      } else {
        print('_toxcitity ' +
            _toxicity.toStringAsFixed(2) +
            ' minus ' +
            number.toStringAsFixed(2) +
            ' = ');
        //_toxicity -= number;
      }
    });
    print('Toxicity bar:' + _toxicity.toStringAsFixed(2));
  }
}
