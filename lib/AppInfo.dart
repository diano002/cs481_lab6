import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:animations/animations.dart';

class AppInfo extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blueGrey[300],
      appBar: AppBar(
        centerTitle: true,
        title: Text("App Information",
            style: Theme.of(context).textTheme.headline2),
      ),
      body:  Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: [
          SizedBox(
            height: 5,
          ),
          SizedBox(
            height: 10,
          ),
          Card(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      leading: Icon(
                        Icons.wb_incandescent,
                        size: 40,
                        color: Colors.purpleAccent,
                      ),
                      title: Text(
                        "How do we determine toxicity?",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      //subtitle: Text("Nature and Nurture",
                      // style: Theme.of(context).textTheme.headline4,),
                    ),
                  )
                ],
              )),
          SizedBox(
            height: 10,
          ),
          Card(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      leading: Icon(
                        Icons.access_alarm,
                        size: 40,
                        color: Colors.blue,
                      ),
                      title: Text(
                        "Time",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      subtitle: Text(
                        "Given a week, the body absorbs toxins without having "
                            "the proper amount of time to rid them. Because of "
                            " this, we carefully account for the quantity of "
                            "toxins present in a plant, which has not had time "
                            "to be flushed from the system.",
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                  )
                ],
              )),
          SizedBox(
            height: 10,
          ),
          Card(
              margin: EdgeInsets.only(left: 10, right: 10),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: ListTile(
                      leading: Icon(
                        Icons.account_balance,
                        size: 40,
                        color: Colors.blueGrey,
                      ),
                      title: Text(
                        "Quantity",
                        style: Theme.of(context).textTheme.headline2,
                      ),
                      subtitle: Text(
                        "Depending on how much of the particular plant is "
                            "ingested will also determine the amount of "
                            "toxins still present in the body after a week. "
                            "We use a combination of time and quantity to"
                            " determine the toxic nature of a plant, as input "
                            "by you, the consumer.",
                        style: Theme.of(context).textTheme.headline5,
                      ),
                    ),
                  )
                ],
              )),
          SizedBox(
            height: 5,
          ),
          Expanded(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                Container(
                  //padding: EdgeInsets.all(1.0),
                  child: Card(
                      child: Image.asset('assets/straw.jpg')),
                ),
                Container(
                  //padding: EdgeInsets.all(1.0),
                  child: Card(
                      child: Image.asset('assets/toxic2.jpg')),
                ),
                Container(
                  //padding: EdgeInsets.all(1.0),
                  child: Card(
                      child: Image.asset('assets/peach.jpg')),
                ),

              ],
            ),
          )
        ],
      ),
      );
   // );
  }
}
// PageTransitionSwitcher(
//   transitionBuilder: (
//       Widget child,
//       Animation<double> primaryAnimation,
//       Animation<double> secondaryAnimation,
//       ) {
//     return FadeThroughTransition(
//       fillColor: Colors.red[300],
//       child: child,
//       animation: primaryAnimation,
//       secondaryAnimation: secondaryAnimation,
//     );
//   },
//   child: