import 'package:cs481_toxic/DetailsScreen.dart';
import 'package:cs481_toxic/GeneralToxicity.dart';
import 'package:cs481_toxic/Home.dart';
import 'package:flutter/material.dart';
import 'package:cs481_toxic/Splash.dart';
import 'package:animations/animations.dart';
import 'package:cs481_toxic/AppInfo.dart';

//testing the add
void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        pageTransitionsTheme: PageTransitionsTheme(
          builders: {
            TargetPlatform.android: FadeThroughPageTransitionsBuilder(),
            TargetPlatform.iOS: FadeThroughPageTransitionsBuilder(),
          },
        ),
        brightness: Brightness.light,
        canvasColor: Colors.greenAccent[100],
        accentColor: Colors.orange,
        backgroundColor: Colors.orangeAccent[100],
        primaryColor: Colors.green,
        visualDensity: VisualDensity.adaptivePlatformDensity,
        textTheme: TextTheme(
          headline1: TextStyle(
              fontSize: 72.0, fontWeight: FontWeight.bold, color: Colors.pink),
          headline2: TextStyle(
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
              color: Colors.red[800]),
          headline3: TextStyle(fontSize: 20.0, color: Colors.red[800]),
          headline4: TextStyle(
              fontSize: 20.0, fontWeight: FontWeight.bold, color: Colors.black),
          headline5: TextStyle(fontSize: 16.0, color: Colors.black),
          headline6: TextStyle(
              fontSize: 36.0, fontStyle: FontStyle.italic, color: Colors.brown),
          subtitle2: TextStyle(
              fontSize: 20.0,
              fontStyle: FontStyle.normal,
              color: Colors.red[900]),
        ),
      ),
      initialRoute: '/',
      routes: {
        //   '/' : (context)  => Splash(),
        '/home': (context) => Home(),
        '/general': (context) => GeneralToxicity(),
        '/appinfo': (context) => AppInfo(),
        '/details/cherry': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Cherry',
              image: 'cherry',
              desc: PlantInfo.details[0],
              toxicity: .2,
              portionSize: 10,
            )),
        '/details/star': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Starfruit',
              image: 'star',
              desc: PlantInfo.details[1],
              toxicity: .3,
              portionSize: 1,
            )),
        '/details/peach': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Peach',
              image: 'peach2',
              desc: PlantInfo.details[2],
              toxicity: .2,
              portionSize: 1,
            )),
        '/details/potato': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Potato',
              image: 'potato',
              desc: PlantInfo.details[3],
              toxicity: .3,
              portionSize: 1,
            )),
        '/details/rhubarb': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Rhubarb',
              image: 'rhubarb',
              desc: PlantInfo.details[4],
              toxicity: .4,
              portionSize: 2,
            )),
        '/details/tomato': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Tomato',
              image: 'tomato',
              desc: PlantInfo.details[5],
              toxicity: .1,
              portionSize: 1,
            )),
        '/details/celery': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Celery',
              image: 'celery',
              desc: PlantInfo.details[6],
              toxicity: .1,
              portionSize: 2,
            )),
        '/details/spinach': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Spinach',
              image: 'spinach',
              desc: PlantInfo.details[7],
              toxicity: .2,
              portionSize: 10,
            )),
        '/details/apple': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Apple',
              image: 'apple2',
              desc: PlantInfo.details[9],
              toxicity: .1,
              portionSize: 1,
            )),
        '/details/elder': (context) => DetailsScreen(
                plant: DetailsPlant(
              plantName: 'Elderberry',
              image: 'elder',
              desc: PlantInfo.details[8],
              toxicity: .4,
              portionSize: 5,
            )),
      },
      home: Splash(),
    );
  }
}

class PlantInfo {
  static const List<String> details = [
    """
    The hard stone in the center of cherries is full 
    of prussic acid, also known as cyanide, which is 
    poisonous. Cherries are also sprayed with pesticides.""",
    """
    The substances found in starfruit can affect the brain 
    and cause neurological disorders. This toxic substance 
    is called a neurotoxin. People with kidneys disease cannot
    process the toxin out from their body.""",
    """
    More than sixty pesticides have been found on peaches. 
    The seeds, also known as pits, do contain a compound 
    called amygdalin. This compound breaks down into 
    hydrogen cyanide when ingested, which is a poison.""",
    """
    Depending on how much is consumed, solanine, the 
    green, poisonous part of the tuber can have effects 
    ranging from mild discomfort to abdominal pain, nausea, 
    diarrhea, vomiting, drowsiness, mental confusion, 
    hortness of breath. Plus, they are heavily sprayed.""",
    """
    The leaves are high in oxalic acid, which can cause 
    kidney problems. Consuming a large number of these 
    leaves can cause all sorts of health problems like 
    stomach aches, severe breathing problems, nausea, 
    and ultimately death.""",
    """
    The unripe fruit, the leaves, roots, and stem are 
    rich in tomatine, an alkaloid that’s mildly toxic 
    to humans, likely to cause you some gastrointestinal 
    distress. The main concern is that it is heavily 
    sprayed with pesticides and herbicides.""",
    """
    Celery can act as a goitrogen if eaten in very 
    large quantities, especially if it's not cooked, which
    interferes with thyroid function. The juice on the skin 
    can cause rashes and irritation in the sun. The plant
    is heavily sprayed with pesticides. """,
    """
    Spinach is high in oxalates, and their excess intake 
    over a period may lead to the formation of kidney stones. 
    The vitamin K in spinach may also interfere with blood 
    thinners and certain other medications. Spinach is heavily
    sprayed with pesticides if not organic.""",
    """
    The entire plant outside of the ripened cooked berries 
    is poisonous for humans to eat. Eating the bark, 
    leaves, roots, and unripe berries will make you nauseous 
    and develop severe stomach aches.""",
    """
    Apple seeds have cyanide, so throwing back a handful as
    a snack isn’t smart. Luckily, apple seeds have a protective
    coating that keeps the cyanide from entering your system. 
    However, apples are heavily sprayed with pesticides. """
  ];
}
