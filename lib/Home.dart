import 'package:cs481_toxic/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:animations/animations.dart';
import 'package:cs481_toxic/DetailsScreen.dart';

import 'GeneralToxicity.dart';

class Home extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        centerTitle: true,
        title: Text("How Toxic IS This?", style: Theme.of(context).textTheme.headline2),
        actions: [
          PopupMenuButton(
              color: Theme.of(context).canvasColor,
              onSelected: (choiceAction) {
            if (choiceAction == Constants.General) {
              print("General");
              Navigator.of(context).pushNamed('/general');
            } else {
              print("App Info");
              Navigator.of(context).pushNamed('/appinfo');
            }
          }, itemBuilder: (BuildContext context) {
            return Constants.choices
                .map((e) => PopupMenuItem(value: e, child: Text(e)))
                .toList();
          })
        ],
      ),
      body: Padding(
          padding: EdgeInsets.all(4),
          child: Container(
            child: StaggeredGridView.count(
              crossAxisCount: 4,
              mainAxisSpacing: 4,
              crossAxisSpacing: 4,
              staggeredTiles: [
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
                StaggeredTile.count(2, 2),
              ],
              children: <Widget>[
                Card(
                    child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/cherry.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Cherry", style: Theme.of(context).textTheme.subtitle2))
                          ],
                        ),
                      ),
                      onTap:() {
                        Navigator.of(context).pushNamed('/details/cherry');
                          }
                    ),
                    elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/star.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Starfruit", style: Theme.of(context).textTheme.subtitle2))
                           // Center(child: Text("Plant 2", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/star');
                      }
                  ),
                  elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/peach2.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Peach", style: Theme.of(context).textTheme.subtitle2))
                            //Center(child: Text("Plant 3", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/peach');
                      }
                  ),
                  elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/potato.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Potato", style: Theme.of(context).textTheme.subtitle2))
                            //Center(child: Text("Plant 4", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/potato');
                      }
                  ),
                  elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/rhubarb.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Rhubarb", style: Theme.of(context).textTheme.subtitle2))
                            //Center(child: Text("Plant 5", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/rhubarb');
                      }
                  ),
                  elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/tomato.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Tomato", style: Theme.of(context).textTheme.subtitle2))
                            //Center(child: Text("Plant 6", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/tomato');
                      }
                  ),
                  elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/celery.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Celery", style: Theme.of(context).textTheme.subtitle2))
                           // Center(child: Text("Plant 7", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/celery');
                      }
                  ),
                  elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/spinach.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Spinach", style: Theme.of(context).textTheme.subtitle2))

                            //Center(child: Text("Plant 8", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/spinach');
                      }
                  ),
                  elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/elder.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Elderberry", style: Theme.of(context).textTheme.subtitle2))
                            //Center(child: Text("Plant 8", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/elder');
                      }
                  ),
                  elevation: 10,),
                Card(
                  child:new InkWell(
                      child: new Expanded(
                        child:Stack(
                          children: [
                            Image.asset("assets/apple2.jpg"),
                            Align(
                                alignment: Alignment.topLeft,
                                child: Text(" Apple", style: Theme.of(context).textTheme.subtitle2))
                            //Center(child: Text("Plant 8", style: TextStyle(fontSize: 35,color: Colors.black)))
                          ],
                        ),
                      ),
                      onTap:() {
                        //Move to new screen
                        Navigator.of(context).pushNamed('/details/apple');
                      }
                  ),
                  elevation: 10,),


              ],
            ),
          )),
    );
  }

  String choiceAction(String choice) {
    if (choice == Constants.General) {
      return Constants.General;
    } else {
      return Constants.AppInfo;
    }
  }
}

class Constants {
  static const String General = 'General Toxicity';
  static const String AppInfo = 'App Information';

  static const List<String> choices = <String>[General, AppInfo];
}
